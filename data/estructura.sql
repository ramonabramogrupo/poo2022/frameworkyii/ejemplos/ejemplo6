﻿DROP DATABASE IF EXISTS ejemplo6yii;
CREATE DATABASE IF NOT EXISTS ejemplo6yii;
USE ejemplo6yii;

CREATE TABLE prendas(
  id int AUTO_INCREMENT,
  titulo varchar(100),
  referencia varchar(100),
  precio float,
  foto varchar(100) NOT NULL,
  portada boolean,
  oferta boolean,
  descuento float,
  categoria int NOT NULL,
  PRIMARY KEY(id)
  );

CREATE TABLE fotos(
  id int AUTO_INCREMENT,
  ruta varchar(100) NOT NULL,
  idprenda int,
  PRIMARY KEY (id)
  );

CREATE TABLE categorias(
  id int AUTO_INCREMENT, 
  tipo varchar(100) NOT NULL,
  subtipo varchar(100),
  PRIMARY KEY(id),
  UNIQUE KEY (tipo,subtipo)
  );


CREATE TABLE caracteristicas(
    id int AUTO_INCREMENT,
    prenda int NOT NULL,
    caracteristica varchar(255) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE KEY (prenda,caracteristica)
  );

ALTER TABLE prendas 
  ADD CONSTRAINT fkPrendasCategorias FOREIGN KEY(categoria)
  REFERENCES categorias(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE caracteristicas 
  ADD CONSTRAINT fkCaracteristicasPrendas FOREIGN KEY(prenda) 
  REFERENCES prendas(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE fotos 
  ADD CONSTRAINT fkFotosPrendas FOREIGN KEY(idprenda)
  REFERENCES prendas(id) ON DELETE CASCADE ON UPDATE CASCADE;

