<?php

namespace app\controllers;

use app\models\Caracteristicas;
use app\models\Fotos;
use app\models\Prendas;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /** mostrando  un array enumerado
     *  utilizando Html::ul y un foreach
     */
    public function actionEjercicio1() {

        // creo el array  
        $datos = [];
        
        // introduzcon en el array numeros
        // desde el 0 al 100
        for ($c = 0; $c < 100; $c++) {
            $datos[$c] = $c;
            //array_push($datos,$c);
        }

        // mostrar el array
        // enumerado de numeros
        // para mostrra contenido se lo mando a la vista
        return $this->render("ejercicio1", [
                    "datos" => $datos
        ]);
    }

    /**
     * Mostrar un array enumerado de arrays asociativos
     * Para realizarlo utilizo un Html::ul
     * y tambien lo realizo con foreach
     * Creo una subvista para mostrar cada registro
     */
    public function actionEjercicio2() {
        // creo el array enumerado de arrays 
        // asociativos (bidimensional)
        
        $visitas = [
            [
                "mes" => "Mayo",
                "visitas" => 10000
            ],
            [
                "mes" => "Junio",
                "visitas" => 100000
            ],
            [
                "mes" => "Julio",
                "visitas" => 200000
            ],
            [
                "mes" => "Agosto",
                "visitas" => 300000
            ],
        ];
        
        //para mostrar el array enumerado de arrays asociativos
        // utilizo en la vista Html::ul o foreach
        
        return $this->render("ejercicio2",[
            "visitas" => $visitas
        ]);
    }
    
    /**
     * Mostrar un array bidimensional es decir un array enumerado de 
     * arrays asociativos utilizando un widget denominado GRIDVIEW
     * para mostrarlo
     */
    
    public function actionEjercicio3(){
         
        // creo el array bidimensional
        $visitas = [
            [
                "mes" => "Mayo",
                "visitas" => 10000
            ],
            [
                "mes" => "Junio",
                "visitas" => 100000
            ],
            [
                "mes" => "Julio",
                "visitas" => 200000
            ],
            [
                "mes" => "Agosto",
                "visitas" => 300000
            ],
        ];
        
        // mostrar el contenido del array en una vista utilizando GRIDVIEW
        
        //primero creo el proveedor de datos
        $dataProvider=new ArrayDataProvider([
            "allModels" => $visitas
        ]);
        
        
        // mando el proveedor de datos a la vista
        return $this->render("ejercicio3",[
            "dataProvider" => $dataProvider,
        ]);
                
    }
    
    /**
     * Mostrar un array bidimensional es decir un array enumerado de 
     * arrays asociativos utilizando un widget denominado GRIDVIEW
     * para mostrarlo
     */
    public function actionEjercicio4(){
        // array enumerado de arrays asociativos
        
        $ventas=[
            [
                "idVenta" => 1,
                "cantidad" =>100,
                "producto" => 56,
                "precioTotal" => 560,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 2,
                "cantidad" =>200,
                "producto" => 53,
                "precioTotal" => 5600,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 3,
                "cantidad" =>200,
                "producto" => 50,
                "precioTotal" => 1600,
                "poblacion" => "Santander",
            ]
        ];
        
        //convierto el array en un proveedor de datos
        $dataProvider= new ArrayDataProvider([
            "allModels" => $ventas
        ]);
        
        // mando el proveedor de datos a la vista
        // que utilizando un widget (GRIDVIEW) muestra los datos
        return $this->render("ejercicio4",[
            "dataProvider" => $dataProvider
        ]);
                
    }
    
    /**
     * Mostrar un array bidimensional es decir un array enumerado de 
     * arrays asociativos utilizando un widget denominado LISTVIEW
     * para mostrarlo
    */
        
    public function actionEjercicio5(){
       // creo el array 
        $visitas = [
            [
                "mes" => "Mayo",
                "visitas" => 10000
            ],
            [
                "mes" => "Junio",
                "visitas" => 100000
            ],
            [
                "mes" => "Julio",
                "visitas" => 200000
            ],
            [
                "mes" => "Agosto",
                "visitas" => 300000
            ],
        ];
        
        
        // mostrar el array con el widget ListView
        
        // crear un dataProvider
        $dataProvider = new ArrayDataProvider([
            "allModels" => $visitas
        ]);
        
        // llamo a la vista y le paso el dataprovider
        // para que lo utilice en el widget ListView
        return $this->render("ejercicio5",[
            "dataProvider" => $dataProvider
        ]);
        
    }
    
    /**
     * Mostrar un array bidimensional es decir un array enumerado de 
     * arrays asociativos utilizando un widget denominado LISTVIEW
     * para mostrarlo
    */
    
    public function actionEjercicio6(){
          // array enumerado de arrays asociativos
        
        $ventas=[
            [
                "idVenta" => 1,
                "cantidad" =>100,
                "producto" => 56,
                "precioTotal" => 560,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 2,
                "cantidad" =>200,
                "producto" => 53,
                "precioTotal" => 5600,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 3,
                "cantidad" =>200,
                "producto" => 50,
                "precioTotal" => 1600,
                "poblacion" => "Santander",
            ]
        ];
        
        $dataProvider=new ArrayDataProvider([
            "allModels" => $ventas
        ]);
        
        return $this->render("ejercicio6",[
            "dataProvider" => $dataProvider
        ]);
    }
    
    
    /**
     * Mostrar un array bidimensional es decir un array enumerado de 
     * arrays asociativos utilizando un widget denominado GRIDVIEW
     * para mostrarlo.
     * 
     * El Array Bidimensional lo saco de una Peticion a un 
     * servidor API REST
     */
    
    public function actionEjercicio7(){
        // api key 
        // 73213b0e77d5466796b0cdb49808e28c
        
        // direccion del servidor api rest donde voy a solicitar datos
        // GET
        $url="https://newsapi.org/v2/everything?q=php&language=es&apiKey=73213b0e77d5466796b0cdb49808e28c";
        
        // lee el contenido de un fichero
        // leer como string los datos solicitados
        $contenido=file_get_contents($url);
        
        // convierte el texto en formato JSON
        // en un objeto de php
        $resultado=json_decode($contenido);
        
        // del objeto leo la propiedad articles
        // que es un array enumerado de arrays asociativos
        $noticias=$resultado->articles;
        
        // crear un dataprovider desde un array
        $dataProvider=new ArrayDataProvider([
            "allModels" => $noticias
        ]);
        
        // mando el dataProvider a la vista
        // para que el widget GRIDVIEW lo muestre
        return $this->render("ejercicio7",[
            "dataProvider" => $dataProvider
        ]);
    
    }

    /**
     * Mostrar un array bidimensional es decir un array enumerado de 
     * arrays asociativos utilizando un widget denominado GRIDVIEW
     * para mostrarlo.
     * 
     * El Array Bidimensional lo saco de una Peticion a un 
     * servidor API REST
     */
    
    public function actionEjercicio8(){
        // api key 
        // 73213b0e77d5466796b0cdb49808e28c
        
        // colocar todas las noticias en un gridview con este formato
        // numero orden, fecha de publicacion, foto, title, autor de la noticia, enlace 
        $url="https://newsapi.org/v2/top-headlines?language=es&apiKey=73213b0e77d5466796b0cdb49808e28c";
        
        $contenido= file_get_contents($url);
        
        $resultado= json_decode($contenido);
        
        $noticias=$resultado->articles;
        
        $dataProvider=new ArrayDataProvider([
            "allModels" => $noticias
        ]);
        
        return $this->render("ejercicio8",[
            "dataProvider" => $dataProvider
        ]);
                
    }
    
    // a partir de aqui comenzamos a trabajar con la base de datos
    // ejemplo6yii
    
    // para poder utilizar las siguientes acciones
    // tengo que tener un modelo de cada tabla
    // utilizando gii
    
    /** 
     * Mostrar el contenido de una tabla (Fotos) en un GRIDVIEW
     * Para ello voy a utilizar un ActiveQuery (consulta activa)
     * Para crear una consulta activa necesito un modelo activo (Active Record)
     * de la tabla Fotos
     * Para poder visualizar el active Query en un GRIDVIEW tengo que crear un DATAPROVIDER
     * Para crear un DATAPROVIDER utilizamos un ActiveDataProvider
     */
    
    public function actionEjercicio9(){
        
        // mostrar la siguiente consulta en un GRIDVIEW
        // select * from fotos (listar todos los registros)
        
        // activeQuery
        $query= Fotos::find();
        
        // CREO EL DATAPROVIDER
        $dataProvider=new ActiveDataProvider([
                "query" => $query
                
        ]);
        
        // MANDO EL DATAPROVIDER A LA VISTA
        return $this->render("ejercicio9",[
            "dataProvider" => $dataProvider
        ]);
    }
    
    /**
     * Modificar uno de las fotos utilizando ActiveRecord
     * 
     */
    
    public function actionEjercicio10(){
        
        // activeQuery
        $query= Fotos::find();
                
        //array de activeRecords (array de modelos)
        // SELECT * FROM FOTOS
        // EL MODELO MANTIENE UNA CONEXION CON LA BBDDD
        $registros=$query->all();
        
        //al tener un array de modelos puedo modificar los registros desde el array
           $registros[0]->idprenda= random_int(1, 5);
           $registros[0]->save(); // MODIFICA LA TABLA FOTOS
           
        return $this->render("ejercicio10",[
            "query" => $query,
            "registros" => $registros
        ]);
    }
    
    /**
     * Mostrar un registro de la tabla fotos 
     * utilizando widget DetailView
     * 
     * Para que el DetailView funcione solo necesito un modelo
     * de ActiveRecord o un modelo o un array Asociativo
     *  
     */
    
    
    public function actionEjercicio11(){
        
        // activeQuery
        // consulta activa
        $query= Fotos::find();
        
        //activeRecord (modelo)
        // sacando el primer registro
        // sacando la primera foto
        $primeraFoto=$query->one(); // select * from fotos limit 1
        
        // array de modelos
        // sacando todas las fotos
        
        $todasFotos=$query->all(); // select * from fotos;
        
        // modelo 2
        // del array de modelos me quedo con la segunda foto
        
        $segundaFoto=$todasFotos[1]; // me quedo con la segunda foto
        
        // mostrar el registro
        // en un DetailView
        // necesito un modelo
        
        return $this->render("ejercicio11",[
            "model" => $segundaFoto, // a la vista le mando un modelo de foto
            //"model" => $primeraFoto, // a la vista le mando un modelo de foto
        ]);
        
        
    }
    
    
    /** 
     * Mostrar el contenido de una tabla (Fotos) en un GRIDVIEW
     * Para ello voy a utilizar el metodo CreateCommand del objeto de acceso a datos
     * Al ejecutar el metodo queryAll() te crea un array bidimensional
     * Mostrar ese array en el GRIDVIEW creando un dataProvider
     * 
     */
    
    
    public function actionEjercicio12(){
        // mostrar la siguiente consulta en un GRIDVIEW
        // select * from fotos
         
        
        // array de arrays asociativos
        $resultados=Yii::$app
                        ->db // objeto de acceso a datos
                        ->createCommand("select * from fotos") // comando sql preparado para ejecutarse
                        ->queryAll(); // consulta ejecutada
        
          // si modifico alguno de los registros no afecta a la tabla
        
        $dataProvider=new ArrayDataProvider([
            "allModels" => $resultados
        ]);
        
        // paso el dataprovider a la vista y lo muestro con un GRIDVIEW
        return $this->render("ejercicio12",[
            "dataProvider" => $dataProvider
        ]);
        
    }
    
    /**
     * Mostrar la tabla fotos utilizando el SqlDataProvider
     * Solo necesito el codigo SQL de la consulta a ejecutar
     * 
     */
    
    
    public function actionEjercicio13(){
        // mostrar la siguiente consulta en un GRIDVIEW
        // select * from fotos
        
        $dataProvider=new SqlDataProvider([
            "sql" => "select * from fotos", // codigo sql de la consulta
        ]);
        
        // mando a la vista el proveedor de datos generado
        return $this->render("ejercicio12",[
            "dataProvider" => $dataProvider
        ]);
        
        
    }
    
    /**
     * listado de todas las prendas utilizando 
     * createcommand
     * SELECT * FROM PRENDAS
     * UTILIZAR PARA MOSTRAR LOS RESULTADOS UN GRIDVIEW
     */
    public function actionEjercicio14(){
//        $resultados=\Yii::$app
//                    ->db // conexion
//                    ->createCommand("select * from prendas") // consulta
//                    ->queryAll(); // consulta ejecutada
        
        // conexion con la bbdd
        $conexion= Yii::$app->db;
        
        // consulta a ejecutar
        $query=$conexion->createCommand("select * from prendas");
        
        // consulta ejecutada
        // devuelve los resultados en un array de arrays
        $resultados=$query->queryAll(); 
        
        // creo un proveedor de datos para mandar el resultado
        // a un gridview
        $dataProvider=new ArrayDataProvider([
            "allModels" => $resultados
        ]);
        
        // mando el proveedor de datos a la vista
        // en la vista colocare un gridview
        return $this->render("ejercicio14",[
           "dataProvider" => $dataProvider 
        ]);
        
    }
    
     /**
     * listado de todas las prendas utilizando 
     * SQLDATAPROVIDER
     * SELECT * FROM PRENDAS
     * UTILIZAR PARA MOSTRAR LOS RESULTADOS UN GRIDVIEW
     */
    public function actionEjercicio15(){
    
        // creo un proveedor de datos
        // para mostrar los datos de la tabla
        // en un widget GRIDVIEW
        $dataProvider=new SqlDataProvider([
            "sql"=>"select * from prendas"
        ]);
        
        // mando el proveedor de datos a la vista
        // en la vista colocare un gridview
        return $this->render("ejercicio14",[
           "dataProvider" => $dataProvider 
        ]);  
        
        
    }
    
    
     /**
     * listado de todas las prendas utilizando 
     * ACTIVEDATAPROVIDER
     * DEBEIS CREAR UN ACTIVEQUERY
     * UTILIZAR PARA MOSTRAR LOS RESULTADOS UN GRIDVIEW
     */
    public function actionEjercicio16(){
        
        // consulta activa 
        // he creado un activeQuery de prendas
        $query= Prendas::find();
        
        // crear un dataProvider desde un
        // activeQuery
        
        $dataProvider=new ActiveDataProvider([
            "query" => $query
        ]);
        
        // mando el proveedor de datos a la vista
        // en la vista colocare un gridview
        return $this->render("ejercicio14",[
           "dataProvider" => $dataProvider 
        ]);
        
        
    }
    
    /**
     * generar un array con el campo caracteristicas
     * de la tabla caracteristicas
     */
    public function actionEjercicio17(){
        // consulta activa
        $query= Caracteristicas::find();
        
        
        // array de modelos
        // hay conexion con la tabla
        $vector=$query->all();
        
        // realizar un array de las caracteristicas con foreach
        /*foreach($vector as $elemento){
            $salida[]=$elemento->caracteristica;
        }*/
        
        // realizar un array de las caracteristicas con ArrayHelper
        $salida=ArrayHelper::getColumn($vector, "caracteristica");
        
        
        
        // array de arrays
        // no hay conexion con la tabla
        $vector1=$query->asArray()->all();
        var_dump($vector1);
        
        // realizar una array con solo el campo caracteristica
        $salida=ArrayHelper::getColumn($vector1, "caracteristica");
        var_dump($salida);
        
    }
    
    
}
