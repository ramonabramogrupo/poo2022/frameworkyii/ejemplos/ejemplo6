<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prendas".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $referencia
 * @property float|null $precio
 * @property string $foto
 * @property int|null $portada
 * @property int|null $oferta
 * @property float|null $descuento
 * @property int $categoria
 *
 * @property Caracteristicas[] $caracteristicas
 * @property Categorias $categoria0
 * @property Fotos[] $fotos
 */
class Prendas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prendas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio', 'descuento'], 'number'],
            [['foto', 'categoria'], 'required'],
            [['portada', 'oferta', 'categoria'], 'integer'],
            [['titulo', 'referencia', 'foto'], 'string', 'max' => 100],
            [['categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::class, 'targetAttribute' => ['categoria' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'referencia' => 'Referencia',
            'precio' => 'Precio',
            'foto' => 'Foto',
            'portada' => 'Portada',
            'oferta' => 'Oferta',
            'descuento' => 'Descuento',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * Gets query for [[Caracteristicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCaracteristicas()
    {
        return $this->hasMany(Caracteristicas::class, ['prenda' => 'id']);
    }

    /**
     * Gets query for [[Categoria0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria0()
    {
        return $this->hasOne(Categorias::class, ['id' => 'categoria']);
    }

    /**
     * Gets query for [[Fotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFotos()
    {
        return $this->hasMany(Fotos::class, ['idprenda' => 'id']);
    }
    
}
