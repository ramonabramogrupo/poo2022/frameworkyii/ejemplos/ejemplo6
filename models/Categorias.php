<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property int $id
 * @property string $tipo
 * @property string|null $subtipo
 *
 * @property Prendas[] $prendas
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo', 'subtipo'], 'string', 'max' => 100],
            [['tipo', 'subtipo'], 'unique', 'targetAttribute' => ['tipo', 'subtipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'subtipo' => 'Subtipo',
        ];
    }

    /**
     * Gets query for [[Prendas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrendas()
    {
        return $this->hasMany(Prendas::class, ['categoria' => 'id']);
    }
}
