<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Prendas $model */

$this->title = 'Create Prendas';
$this->params['breadcrumbs'][] = ['label' => 'Prendas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prendas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
