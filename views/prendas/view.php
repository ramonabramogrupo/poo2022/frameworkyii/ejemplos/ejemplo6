<?php

use app\models\Prendas;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/** @var View $this */
/** @var Prendas $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Prendas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="prendas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'titulo',
            'referencia',
            'precio',
            //'foto',
            [
                'label' => 'Foto Prenda',
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function($modelo){
                    return Html::img(
                            "@web/imgs/" . $modelo->foto,
                            ['width'=>'100']
                            );
                }
            ],
            //'portada',
            //'oferta',
            [
                'attribute' => 'oferta',
                'value' => function($modelo){
                    if($modelo->oferta){
                        return "Si";
                    }else{
                        return "No";
                    }
                }
            ],
            'descuento',
            //'categoria',
            'categoria0.tipo',
            'categoria0.subtipo',
            [
                'label' => 'Caracteristicas',
                'format' => 'raw',
                'value' => function($modelo){
                    return Html::ul(
                            ArrayHelper::getColumn(
                                $modelo->caracteristicas,
                                'caracteristica'
                                )
                            );
                }
            ]
        ],
    ]) ?>
    
</div>
