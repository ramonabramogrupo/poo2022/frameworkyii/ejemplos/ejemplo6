<?php

use app\models\Prendas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Prendas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prendas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Prendas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'referencia',
            'precio',
            [
                'label' => 'Foto Prenda',
                'attribute' => 'foto',
                'content' => function($modelo){
                    return Html::img(
                            "@web/imgs/" . $modelo->foto,
                            ['width'=>'100']
                            );
                }
            ],
            //'portada',
            'oferta',
            'descuento',
            'categoria',
            'categoria0.tipo',
            'categoria0.subtipo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Prendas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
