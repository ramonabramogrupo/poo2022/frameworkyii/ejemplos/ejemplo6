<?php

use yii\helpers\Html;

// utilizando un foreach
//echo "<div>";
//foreach ($visitas as $registro){
//    // llamo a la vista por cada registro
//    echo $this->render("_ejercicio2",[
//       "registro" => $registro 
//    ]);
//}
//echo "</div>";

// utilizando html::ul

echo Html::ul($visitas,[
    "item" =>function($registro){
        return $this->render("_ejercicio2",[
           // mando a la subvista un array asociativo
           // con el registro a mostrar
           "registro" => $registro 
        ]);
    }
]);


