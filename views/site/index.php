<?php

use yii\helpers\Html;
use yii\web\View;
/* @var $this View */

$this->title = 'Visualizacion de arrays y CRUD de tablas';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo 6</h1>

        <p class="lead">Visualizacion de Arrays y CRUD de tablas</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Ejercicio1</h2>

                <div>
                    Mostrar en pantalla los números del 1 al 100
                    <ul>
                        <li>Creamos un array con los números del 1 al 100</li>
                        <li>Mandamos a la vista el array para que lo muestre</li>
                        <li>En la vista quiero que mostréis el array con un Html::ul</li>
                        <li>En la vista quiero que mostremos el array con foreach</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio1", // label del boton
                        ["site/ejercicio1"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 2</h2>

                <div>
                    Mostrar un array bidimensional (array enumerado de arrays asociativos)
                    <ul>
                        <li>Creamos un array enumerado de arrays asociativos</li>
                        <li>Mandamos a la vista el array para que lo muestre</li>
                        <li>En la vista quiero que mostréis el array con un Html::ul</li>
                        <li>En la vista quiero que mostremos el array con foreach</li>
                        <li>En la vista tengo que utilizar una subvista</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio2", // label del boton
                        ["site/ejercicio2"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 3</h2>

                <div>
                    Mostrar un array bidimensional (array enumerado de arrays asociativos)
                    <ul>
                        <li>Creamos un array enumerado de arrays asociativos</li>
                        <li>Mandamos a la vista el array para que lo muestre</li>
                        <li>En la vista quiero que mostréis el array con GRIDVIEW</li>
                        <li>Para ello necesito crear un DATAPROVIDER</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ARRAYDATAPROVIDER</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio3", // label del boton
                        ["site/ejercicio3"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 4</h2>

                <div>
                    Mostrar un array bidimensional (array enumerado de arrays asociativos)
                    <ul>
                        <li>Creamos un array enumerado de arrays asociativos</li>
                        <li>Mandamos a la vista el array para que lo muestre</li>
                        <li>En la vista quiero que mostréis el array con GRIDVIEW</li>
                        <li>Para ello necesito crear un DATAPROVIDER</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ARRAYDATAPROVIDER</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio4", // label del boton
                        ["site/ejercicio4"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 5</h2>

                <div>
                    Mostrar un array bidimensional (array enumerado de arrays asociativos)
                    <ul>
                        <li>Creamos un array enumerado de arrays asociativos</li>
                        <li>Mandamos a la vista el array para que lo muestre</li>
                        <li>En la vista quiero que mostréis el array con LISTVIEW</li>
                        <li>Para ello necesito crear un DATAPROVIDER</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ARRAYDATAPROVIDER</li>
                        <li>El ListView necesita una subvista para mostrar cada registro</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio5", // label del boton
                        ["site/ejercicio5"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 6</h2>

                <div>
                    Mostrar un array bidimensional (array enumerado de arrays asociativos)
                    <ul>
                        <li>Creamos un array enumerado de arrays asociativos</li>
                        <li>Mandamos a la vista el array para que lo muestre</li>
                        <li>En la vista quiero que mostréis el array con LISTVIEW</li>
                        <li>Para ello necesito crear un DATAPROVIDER</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ARRAYDATAPROVIDER</li>
                        <li>El ListView necesita una subvista para mostrar cada registro</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio6", // label del boton
                        ["site/ejercicio6"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 7</h2>

                <div>
                    Solicitar datos al servidor API REST y mostrarlos en un GRIDVIEW
                    <ul>
                        <li>Solicitar mediante file_get_contents noticias</li>
                        <li>Convertimos el JSON en un array bidimensional (json_decode)</li>
                        <li>Creamos un dataProvider</li>
                        <li>Mostrarmos en la vista el DataProvider con un GRIDVIEW</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ARRAYDATAPROVIDER</li>
                        <li>Modificamos el GRIDVIEW para mostrar imagenes y enlaces</li>
                        
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio7", // label del boton
                        ["site/ejercicio7"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 8</h2>

                <div>
                    Solicitar datos al servidor API REST y mostrarlos en un GRIDVIEW
                    <ul>
                        <li>Solicitar mediante file_get_contents noticias</li>
                        <li>Convertimos el JSON en un array bidimensional (json_decode)</li>
                        <li>Creamos un dataProvider</li>
                        <li>Mostrarmos en la vista el DataProvider con un GRIDVIEW</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ARRAYDATAPROVIDER</li>
                        <li>Modificamos el GRIDVIEW para mostrar imagenes y enlaces</li>
                        
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio8", // label del boton
                        ["site/ejercicio8"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 9</h2>

                <div>
                    Mostrar la tabla fotos a traves de un GRIDVIEW
                    <ul>
                        <li>Creamos un active query de fotos</li>
                        <li>Creamos un DATAPROVIDER del activeQuery</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ACTIVEDATAPROVIDER</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio9", // label del boton
                        ["site/ejercicio9"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 10</h2>

                <div>
                    Modificar un registro de fotos utilizando ActiveRecord
                    <ul>
                        <li>Cambiar la prenda de la primera foto de forma aleatoria</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio10", // label del boton
                        ["site/ejercicio10"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 11</h2>

                <div>
                    Mostrar un registro de fotos en un DetailView
                    <ul>
                        <li>Crear un modelo de ActiveRecord para mandarle al Widget</li>
                        <li>Utilizamos el metodo one() de un activeQuery</li>
                        <li>Tambien podemos utilizar el metodo all() de un activeQuery y quedarnos con el registro que queramos</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio11", // label del boton
                        ["site/ejercicio11"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 12</h2>

                <div>
                    Mostrar la tabla fotos en un GRIDVIEW utilizando CreateCommand
                    <ul>
                        <li>Utilizando el objeto de acceso a datos ejecuto una consulta SQL </li>
                        <li>Mando el resultado de la consulta, un array bidimensional al ArrayDataProvider</li>
                        <li>Mando el dataProvider al GRIDVIEDW</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio12", // label del boton
                        ["site/ejercicio12"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 13</h2>

                <div>
                    Mostrar la tabla fotos utilizando el SqlDataProvider en un GRIDVIEW
                    <ul>
                        
                        <li>Creo el DataProvider utilizando la clase SqlDataProvider y el codigo SQL de la consulta</li>
                        <li>Mando el dataProvider al GRIDVIEDW</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio13", // label del boton
                        ["site/ejercicio13"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 14</h2>

                <div>
                    Mostrar la tabla Prendas en un GRIDVIEW utilizando CreateCommand
                    <ul>
                        <li>Utilizando el objeto de acceso a datos ejecuto una consulta SQL </li>
                        <li>Mando el resultado de la consulta, un array bidimensional al ArrayDataProvider</li>
                        <li>Mando el dataProvider al GRIDVIEDW</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio14", // label del boton
                        ["site/ejercicio14"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 15</h2>

                <div>
                    Mostrar la tabla Prendas utilizando el SqlDataProvider en un GRIDVIEW
                    <ul>
                        
                        <li>Creo el DataProvider utilizando la clase SqlDataProvider y el codigo SQL de la consulta</li>
                        <li>Mando el dataProvider al GRIDVIEDW</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio15", // label del boton
                        ["site/ejercicio15"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio 16</h2>

                <div>
                    Mostrar la tabla Prendas a traves de un GRIDVIEW
                    <ul>
                        <li>Creamos un active query de fotos</li>
                        <li>Creamos un DATAPROVIDER del activeQuery</li>
                        <li>Para crear el DATAPROVIDER utilizo la clase ACTIVEDATAPROVIDER</li>
                    </ul>
                </div>

                <p><?= Html::a(
                        "Ejercicio16", // label del boton
                        ["site/ejercicio16"], // controlador/accion
                        ["class"=>"btn btn-primary"] // estilo visual del boton
                        )?></p>
            </div>
        </div>

    </div>
</div>
