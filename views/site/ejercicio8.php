<?php

use yii\grid\GridView;
use yii\helpers\Html;
echo GridView::widget([
    "dataProvider" => $dataProvider,
    "columns" =>[
        ['class' => 'yii\grid\SerialColumn'], // numero orden
        [
            'label' => 'fecha Publicación',
            'attribute' => 'publishedAt'
        ],
        [
            'label' => 'Foto',
            'content' => function ($dato){
                return Html::img($dato->urlToImage,["width"=>200]);
            }
        ],
        'title',
        [
            'label' => 'Autor de la noticia',
            'attribute' => 'author'
        ],
        [
            'label' => 'enlace',
            'content' => function($dato){
                return Html::a(
                        "Ver Noticia",
                        $dato->url,
                        ["class"=>"btn btn-primary"]
                        );
            }
        ]
    ],
]);
