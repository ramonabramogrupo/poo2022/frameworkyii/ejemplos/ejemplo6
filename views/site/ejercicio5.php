<?php

use yii\helpers\Html;
use yii\widgets\ListView;

// boton para llevarnos a la accion ejercicio3
// que me muestra los datos en un GRIDVIEW
echo Html::a(
        "Tabla", //label
        ["site/ejercicio3"], // controlador/accion
        ["class"=>"btn btn-primary m-3"] // clases de bootstrap
        );


// mostrar el LISTVIEW
echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_ejercicio5", // el LISTVIEW necesita un subvista
]);

