<?php

use yii\grid\GridView;
use yii\helpers\Html;

// modificar el GRIDVIEW para mostrar imagenes y enlaces


echo GridView::widget([
    "dataProvider" => $dataProvider,
    "columns" =>[
        ['class' => 'yii\grid\SerialColumn'], // numero orden
        // campos a mostrar
        "author",
        "title",
        // campo al que cambio el label
        [
            'attribute' => 'description',
            'label' => 'Descripcion'
        ],
        
        // campo de imagen
        // un campo que tiene la url de una imagen
        // me coloca la imagen
        [
            'label' => 'Imagen',
            'content' => function($dato){
                return Html::img($dato->urlToImage,["width"=>200]);
            }
        ],
                
        // campo de imagen
        // un campo que tiene la url de una imagen
        // me coloca la imagen
        [
            'label' => 'Imagen 1',
            'format' => 'raw',
            'value' => function($dato){
                return Html::img($dato->urlToImage,["width"=>200]);
            }
        ],   
                
        // campo de url
        // un campo que contiene una URL 
        // y quiero colocar un boton que me permita
        // ir a la URL
        [ 
            'label' => ' Ir a la noticia',
            'content' => function($dato){
                return Html::a(
                        "Ver Noticia completa",
                        $dato->url,
                        ["class"=>"btn btn-primary"]
                    );
            }
        ],
                
    ],
    // estilos visuales utilizando bootstrap
    'tableOptions' =>['class' => 'table table-dark table-striped table-bordered'],
    // cambiando la estructura del gridview
    'layout'=>"{pager}\n{summary}\n{items}\n{summary}\n{pager}",
    
]);


