<?php

use yii\grid\GridView;
use yii\helpers\Html;

// coloco un boton en la vista para mostrar los datos 
// desde un LISTVIEW que realizo en la accion ejercicio5
echo Html::a("Lista",["site/ejercicio5"],['class'=>'btn btn-primary m-3']);

// coloco el mismo boton pero sin utilizar Html::a y utilizando Url::to
?>

<a class="btn btn-primary m-3" href="<?= \yii\helpers\Url::to(["site/ejercicio5"])?>">Lista</a>   

<?php
// colocamos el gridview
echo GridView::widget([
   "dataProvider" => $dataProvider
]);

?> 



