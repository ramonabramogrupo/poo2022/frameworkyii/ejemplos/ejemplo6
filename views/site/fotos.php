<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Fotos $model */
/** @var ActiveForm $form */
?>
<div class="site-fotos">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'ruta') ?>
        <?= $form->field($model, 'idprenda') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-fotos -->
